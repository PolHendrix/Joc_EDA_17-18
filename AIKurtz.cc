#include "Player.hh"



/**
 * Write the name of your player and save this file
 * with the same name and .cc extension.
 */
#define PLAYER_NAME Kurtz


struct PLAYER_NAME : public Player {

  /**
   * Factory: returns a new instance of this class.
   * Do not modify this function.
   */
  static Player* factory () {
    return new PLAYER_NAME;
  }

  /**
   * Types and attributes for your player can be defined here.
   */

   map<int , stack<Position> > Soldat;
   map<int, int> Helicopter;

   typedef map<int, stack<Position> >::iterator it;
   typedef map<int, int>::iterator it2;

   typedef vector<bool> V;
   typedef vector<V> VE;

   typedef vector<Position> P;
   typedef vector<P> PE;

   Position find_close_post(Position ini){ //works
     double min = 20000000000;
     vector<Post> base = posts();
     Position desti;
     Post objective = base[0];
     if(objective.player != me()){
       double distance = (((objective.pos.i - ini.i)*(objective.pos.i - ini.i)) + ((objective.pos.j - ini.j)*(objective.pos.j - ini.j)));
       min = sqrt(distance);
       desti = objective.pos;
     }
     for(int i = 1; i < (int)base.size(); ++i){
       objective = base[i];
       if(objective.player != me()){
         double distance = (((objective.pos.i - ini.i)*(objective.pos.i - ini.i)) + ((objective.pos.j - ini.j)*(objective.pos.j - ini.j)));
         distance = sqrt(distance);
         if(distance < min){
           min = distance;
           desti = objective.pos;
         }
       }
     }
     return desti;
   }



   stack <Position> create_path(Position ini, Position desti, PE caminet){ // works
     stack <Position> path;
     path.push(desti);
     while(caminet[desti.i][desti.j] != ini){
       desti = caminet[desti.i][desti.j];
       if(desti != ini) path.push(desti);
     }
     return path;
   }

   void find_way(int id){ //works
     Data fitxa = data(id);
     Position desti;
     desti = find_close_post(fitxa.pos);
     queue<Position> cami;
     VE map(60, V(60));
     PE caminet(60, P(60));

     for(int i = 0; i < 60; ++i){
       for(int j = 0; j < 60; ++j) map[i][j] = false;
     }
     map[fitxa.pos.i][fitxa.pos.j] = true;
     cami.push(fitxa.pos);
     Position ini = cami.front();
     bool trobat = false;
     while(!cami.empty() and not trobat){
       cami.pop();
       if(ini.i-1 >= 0 and map[ini.i-1][ini.j] == false and  what(ini.i-1, ini.j) != 3 and  what(ini.i-1, ini.j) != 4 and not trobat ){
          if(ini.i-1 == desti.i and ini.j == desti.j) trobat = true;
          map[ini.i-1][ini.j] = true;
          caminet[ini.i-1][ini.j] = Position(ini.i, ini.j);
          cami.push(Position(ini.i-1, ini.j));
       }
       if(ini.i-1 >= 0 and ini.j+1 < 60 and map[ini.i-1][ini.j+1] == false and  what(ini.i-1, ini.j+1) != 3 and  what(ini.i-1, ini.j+1) != 4 and not trobat){
          if(ini.i-1 == desti.i and ini.j+1 == desti.j) trobat = true;
          map[ini.i-1][ini.j+1] = true;
          caminet[ini.i-1][ini.j+1] = Position(ini.i, ini.j);
          cami.push(Position(ini.i-1, ini.j+1));
       }
       if(ini.j+1 < 60 and map[ini.i][ini.j+1] == false and what(ini.i, ini.j+1) != 3 and what(ini.i, ini.j+1) != 4 and not trobat){
          if(ini.i == desti.i and ini.j+1 == desti.j) trobat = true;
          map[ini.i][ini.j+1] = true;
          caminet[ini.i][ini.j+1] = Position(ini.i, ini.j);
          cami.push(Position(ini.i, ini.j+1));
       }
       if(ini.i+1 < 60 and ini.j+1 < 60 and map[ini.i+1][ini.j+1] == false and what(ini.i+1, ini.j+1) != 3 and what(ini.i+1, ini.j+1) != 4 and not trobat ){
          if(ini.i+1 == desti.i and ini.j+1 == desti.j) trobat = true;
          map[ini.i+1][ini.j+1] = true;
          caminet[ini.i+1][ini.j+1] = Position(ini.i, ini.j);
          cami.push(Position(ini.i+1, ini.j+1));
       }
       if(ini.i+1 < 60 and map[ini.i+1][ini.j] == false and what(ini.i+1, ini.j) != 3 and what(ini.i+1, ini.j) != 4 and not trobat ){
          if(ini.i+1 == desti.i and ini.j == desti.j) trobat = true;
          map[ini.i+1][ini.j] = true;
          caminet[ini.i+1][ini.j] = Position(ini.i, ini.j);
          cami.push(Position(ini.i+1, ini.j));
       }
       if(ini.i+1 < 60 and ini.j-1 >= 0 and map[ini.i+1][ini.j-1] == false and what(ini.i+1, ini.j-1) != 3 and what(ini.i+1, ini.j-1) != 4 and not trobat ){
          if(ini.i+1 == desti.i and ini.j-1 == desti.j) trobat = true;
          map[ini.i+1][ini.j-1] = true;
          caminet[ini.i+1][ini.j-1] = Position(ini.i, ini.j);
          cami.push(Position(ini.i+1, ini.j-1));
       }
       if(ini.j-1 >= 0 and map[ini.i][ini.j-1] == false and what(ini.i, ini.j-1) != 3 and what(ini.i, ini.j-1) != 4 and not trobat){
          if(ini.i == desti.i and ini.j-1 == desti.j) trobat = true;
          map[ini.i][ini.j-1] = true;
          caminet[ini.i][ini.j-1] = Position(ini.i, ini.j);
          cami.push(Position(ini.i, ini.j-1));
       }
       if(ini.i-1 >= 0 and ini.j-1 >= 0 and map[ini.i-1][ini.j-1] == false and what(ini.i-1, ini.j-1) != 3 and what(ini.i-1, ini.j-1) != 4 and not trobat ){
          if(ini.i-1 == desti.i and ini.j-1 == desti.j) trobat = true;
          map[ini.i-1][ini.j-1] = true;
          caminet[ini.i-1][ini.j-1] = Position(ini.i, ini.j);
          cami.push(Position(ini.i-1, ini.j-1));
       }
       ini = cami.front();
    }
    it n = Soldat.find(id);
    n->second = create_path(fitxa.pos, desti, caminet);
  }

   void engage(int id) { //works fine
     int player = me();
     it p = Soldat.find(id);
     Data fitxa = data(id);
     int i = fitxa.pos.i;
     int j = fitxa.pos.j;
     if(i-1 >= 0){
        int id2 = which_soldier(i-1, j);
        if(id2 and data(id2).player != player and data(id2).life <= fitxa.life){
          command_soldier(id, i-1, j);
          stack<Position> buida;
          p->second = buida;
          return;
        }
     }
     if(i-1 >= 0 and j+1 < 60){
       int id2 = which_soldier(i-1, j+1);
       if(id2 and data(id2).player != player and data(id2).life <= fitxa.life){
         command_soldier(id, i-1, j+1);
         stack<Position> buida;
         p->second = buida;
         return;
       }
     }
     if(j+1 < 60){
       int id2 = which_soldier(i, j+1);
       if(id2 and data(id2).player != player and data(id2).life <= fitxa.life){
         command_soldier(id, i, j+1);
         stack<Position> buida;
         p->second = buida;
         return;
       }
     }
     if(i+1 < 60 and j+1 < 60){
       int id2 = which_soldier(i+1, j+1);
       if(id2 and data(id2).player != player and data(id2).life <= fitxa.life){
         command_soldier(id, i+1, j+1);
         stack<Position> buida;
         p->second = buida;
         return;
       }
     }
     if(i+1 < 60){
       int id2 = which_soldier(i+1, j);
       if(id2 and data(id2).player != player and data(id2).life <= fitxa.life){
         command_soldier(id, i+1, j);
         stack<Position> buida;
         p->second = buida;
         return;
       }
     }
     if(i+1 < 60 and j-1 >= 0){
       int id2 = which_soldier(i+1, j-1);
       if(id2 and data(id2).player != player and data(id2).life <= fitxa.life){
         command_soldier(id, i+1, j-1);
         stack<Position> buida;
         p->second = buida;
         return;
       }
     }
     if(j-1 >= 0){
       int id2 = which_soldier(i, j-1);
       if(id2 and data(id2).player != player and data(id2).life <= fitxa.life){
         command_soldier(id, i, j-1);
         stack<Position> buida;
         p->second = buida;
         return;
       }
     }
     if(i-1 >= 0 and j-1 >= 0){
       int id2 = which_soldier(i-1, j-1);
       if(id2 and data(id2).player != player and data(id2).life <= fitxa.life){
         command_soldier(id, i-1, j-1);
         stack<Position> buida;
         p->second = buida;
         return;
       }
     }
     Position go = p->second.top();
     if(fire_time(go.i, go.j) == 0){
       command_soldier(id, go.i, go.j);
       fitxa = data(id);
       if(fitxa.pos != go){
         stack<Position> buida;
         p->second = buida;
       }
       else p->second.pop();
     }
   }

   void play_soldier(int id){ //works
     Data fitxa = data(id);
     it p = Soldat.find(id);
     if(p == Soldat.end()){
       stack<Position> cami;
       Soldat.insert(pair<int, stack<Position>>(id, cami));
       find_way(id);
       engage(id);
     }
     else if(p->second.empty()){
       find_way(id);
       engage(id);
     }
     else{
       engage(id);
     }
   }

   void napalm(int id){
     int player = me();
     Data fitxa = data(id);
     Position centre = fitxa.pos;
     int enemics = 0;
     int aliats = 0;
     for(int i = -2; i < 3; ++i){
       for(int j = -2; j < 3; ++j){
         int id2 = which_soldier(centre.i+i, centre.j+j);
         if(id2 and data(id2).player != player) ++enemics;
         else if(id2 and data(id2).player == player) ++aliats;
       }
     }
     if(enemics > aliats and enemics >= 3) command_helicopter(id, NAPALM);
   }

   int cuadrant(int id){
	   Data fitxa = data(id);
	   if(fitxa.pos.i < 30 and fitxa.pos.j < 30) return 0; //north-west
	   else if(fitxa.pos.i < 30 and fitxa.pos.j > 30) return 1; //north-east
	   else if(fitxa.pos.i > 30 and fitxa.pos.j < 30) return 2; //south-west
	   else return 3; //south-east
   }

   void assign_mission(int id, int posicio){
	   vector<int> Heli = helicopters(me());
	   it2 p = Helicopter.find(id);
	   Data fitxa = data(id);
	   if(posicio == 0){
		   if(fitxa.pos.i == 5 and fitxa.pos.j == 15){
			   p->second = 0;
		   }
		   else{
			   p->second = 1;
	   }
   }
	   if(posicio == 1){
		   if(fitxa.pos.i == 5 and fitxa.pos.j == 45){
			   p->second = 0;
		   }
		   else{
			    p->second = 1;
			}
	   }
	   if(posicio == 2){
		   if(fitxa.pos.i == 45 and fitxa.pos.j == 5){
			   p->second = 1;
		   }
		   else{
			    p->second = 0;
			}
	   }
	   if(posicio == 3){
		   if(fitxa.pos.i == 45 and fitxa.pos.j == 55){
			   p->second = 1;
		   }
		   else{
			    p->second = 0;
			}
	   }
   }

   void engage_h(int id){
	   Data fitxa = data(id);
	   it2 p = Helicopter.find(id);
	   if(p->second == 0){
		   if(fitxa.pos.j == 15 and fitxa.orientation != 1){
			   command_helicopter(id, COUNTER_CLOCKWISE);
		   }
       else if(fitxa.pos.j == 45 and fitxa.orientation != 3){
         command_helicopter(id, CLOCKWISE);
       }
       else command_helicopter(id, FORWARD2);
	   }
	   else{
       if(fitxa.pos.i == 15 and fitxa.orientation != 0){
			   command_helicopter(id, CLOCKWISE);
		   }
       else if(fitxa.pos.i == 45 and fitxa.orientation != 2){
         command_helicopter(id, COUNTER_CLOCKWISE);
       }
       else command_helicopter(id, FORWARD2);
	   }
   }

      void throw_parachuter(int helicopter_id) { //need to improve

        Data in = data(helicopter_id);
        Position actual = in.pos;
        int count = 0;
        for(int i = -2; i < 3; ++i){
          for(int j = -2; j < 3; ++j){
            if(count < 4 and not in.parachuters.empty() and what(actual.i+i, actual.j+j) != 3 and fire_time(actual.i+i, actual.j+j) == 0 and which_soldier(actual.i+i, actual.j+j) == 0){
               command_parachuter(actual.i+i, actual.j+j);
               ++count;
             }
          }
        }
      }




   void play_helicopter(int id) { //include new mission strategy and throw parachuter
     Data fitxa = data(id);
     if (fitxa.napalm == 0) {
       napalm(id);
     }
		 it2 p = Helicopter.find(id);
		 if(p == Helicopter.end()){
  		 Helicopter.insert(pair<int, int>(id, -1));
  		 int posicio = cuadrant(id);
  		 assign_mission(id, posicio);
       throw_parachuter(id);
  		 engage_h(id);
		 }
		 else{
       throw_parachuter(id);
       engage_h(id);
	 }
 }


  /**
   * Play method, invoked once per each round.
   */
  virtual void play () { //permutation works
    int player = me();

    vector<int> H = helicopters(player);
    vector<int> S = soldiers(player);
    vector<Post> P = posts();
    vector<int> permh = random_permutation((int)H.size());
    vector<int> perms = random_permutation((int)S.size());

    for (int i = 0; i < (int)H.size(); ++i) play_helicopter(H[permh[i]]);
    for (int i = 0; i < (int)S.size(); ++i) play_soldier(S[perms[i]]);
  }
};


/**
 * Do not modify the following line.
 */
RegisterPlayer(PLAYER_NAME);
